#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#if !defined(TESTING_PUISSANCE4)
#define NOOP ((void)0)
#define afficher_ponderation(a,b,c) NOOP
#else
#include <assert.h>
#endif

#define LARGEUR_DAMIER (10)
#define HAUTEUR_DAMIER (10)
#define TAILLE_DAMIER (LARGEUR_DAMIER * HAUTEUR_DAMIER)

#define LONGUEUR_VICTOIRE_RAW 4
#define LONGUEUR_VICTOIRE (LONGUEUR_VICTOIRE_RAW)
#define NOMBRE_JOUEURS (2)

#define CASE_VIDE ' '
#define CASE_JOUEUR_1 'X'
#define CASE_JOUEUR_2 'O'

#define MACRO_PRESS_ENTER_TO_CONTINUE \
do { \
printf(u8"\n- - Appuyez sur [Entrée] pour continuer - -\n");\
scanf("%*1c%*[^\n]");\
} while(0)

enum cardinaux {
    NORD = 1,
    SUD = 2,
    OUEST = 4,
    EST = 8,
};

enum dirs {
    DIR_N,
    DIR_S,
    DIR_O,
    DIR_E,
    DIR_NO,
    DIR_SE,
    DIR_NE,
    DIR_SO,
    NOMBRE_DIRECTIONS,
};

enum bords {
    BORD_HAUT,
    BORD_DROIT,
    BORD_BAS,
    BORD_GAUCHE,
    NOMBRE_BORDS,
};

struct info_direction {
    int bitmask;
    long delta;
};

typedef struct info_direction voisins[NOMBRE_DIRECTIONS];

struct jeu {
    struct dam {
        struct empla {
            char contenu;
            size_t index;
            size_t ligne;
            size_t colonne;
        } cases[TAILLE_DAMIER];
        voisins voisins;
        int bords[NOMBRE_BORDS];
    } dam;

    struct joueur {
        _Bool humain : 1;
        _Bool gagnant : 1;
        char symbole;
        size_t index;
    } joueurs[NOMBRE_JOUEURS];

    _Bool egalite : 1;
};

/** Fonctions utilitaires */
/* -> 100 pour 2, 1000 pour 3... */
size_t puissance_10(long puissance) {
    size_t mult = 1;

    if (puissance < 0)
        mult = 0;
    else while (puissance--) {
        mult *= 10;
    }

    return mult;
}
/** End */

/** Fonctions servant à respecter le principe de connaissance minimale */
/* -> un pointeur sur un joueur selon son index */
struct joueur * joueur(struct jeu * jeu, size_t index) {
    return &(jeu->joueurs[index]);
}

/* -> une case d'un damier selon son index */
struct empla * empla(struct dam *dam, size_t index) {
    return &dam->cases[index];
}

/* -> la structure qui contient le couple requete/destination
 * pour une direction donnée*/
struct info_direction * info_direction(enum dirs dir, voisins *cibles) {
    return &(*cibles)[dir];
}

/* A partir d'une requete de direction et d'un tableau de cibles,
 * -> le bitmask correspondant a cette direction */
enum cardinaux bitmask_direction(enum dirs dir, voisins *cibles) {
    return info_direction(dir, cibles)->bitmask;
}

/* A partir d'une requete de direction et d'un tableau de cibles,
 * -> difference à ajouter/soustraire dans l'echiquier */
long direction_delta(enum dirs dir, voisins *cibles) {
    return info_direction(dir, cibles)->delta;
}
/** End */

/** Fonctions rendant le code plus lisible */
/* -> pointeur sur le prochain joueur à jouer dans la partie */
struct joueur * prochain_joueur(struct jeu * jeu, struct joueur * jr) {
    return &(jeu->joueurs[(jr->index + 1) % NOMBRE_JOUEURS]);
}

char demander_lettre() {
    char lettre;

    scanf(" %c%*[^\n]", &lettre);

    return lettre;
}

/* Demande d'entrer un nombre entre 0 et INT_MAX, -> la valeur entree convertie en int, -1 si entree invalide */
int demander_nombre_naturel() {
    char string_rec[10];
    int val;

    fgets(string_rec, 10, stdin);
    val = sscanf(string_rec, "%9s%*[^\n]", string_rec);
    if(val == EOF) {
        val = -1;
    }
    else {
        val = (int)strtoul(string_rec, NULL, 10);
    }
    if(val < 0) {
        val = -1;
    }

return val;
}

int poids_est_gagnant(size_t poids) {
    return poids >= puissance_10(LONGUEUR_VICTOIRE - 1);
}
/** End */

/* Fonction à utiliser avec la macro en dessous*/
size_t ___meilleur(
        void * addresse,
        size_t taille_conteneur,
        size_t taille_element,
        size_t decallage) {
    char *int_array_start = (char *)addresse + decallage;
    char *int_array_end = (char *)addresse + taille_conteneur - taille_element + decallage;
    int max_count = 0;
    long cur_max_a = *(long *)int_array_start;
    size_t index = 0, index_max = 0;
    union { char * ic; long * il; } it;

    for (it.ic = int_array_start; it.ic <= int_array_end; it.ic += taille_element) {

        if (*it.il > cur_max_a) {
            cur_max_a = *it.il;
            max_count = 1;
            index_max = index;
        }
        else if (*it.il == cur_max_a) {
            ++max_count;

            if (rand() < RAND_MAX / (max_count + 1)) { // NOLINT(cert-msc30-c,cert-msc50-cpp)
                cur_max_a = *it.il;
                index_max = index;
            }
        }

        ++index;
    }

    return index_max;
}

/* -> l'index de la meilleure valeur d'un aggregat,
 * avec un decallage eventuel pour un aggrégat de structures. */
#define MEILLEURIDX(conteneur, decallage) \
___meilleur(&conteneur, sizeof(conteneur), sizeof(conteneur[0]), decallage)

#define MEILLEUR(conteneur, decallage) conteneur[MEILLEURIDX(conteneur, decallage)]


/**
 * -> la case qui se trouvera vers la direction souhaitée, ou NULL
 * si la case demandée est en dehors des limites du damier
 */
struct empla * vers(struct dam * dam, struct empla * orig, enum dirs dir) {
    voisins * vois = &dam->voisins;
    int bitmask = bitmask_direction(dir, vois);

    if (((orig->ligne <= dam->bords[BORD_HAUT]) && (bitmask & NORD))
        || ((orig->ligne >= dam->bords[BORD_BAS]) && (bitmask & SUD))
        || ((orig->colonne <= dam->bords[BORD_GAUCHE]) && (bitmask & OUEST))
        || ((orig->colonne >= dam->bords[BORD_DROIT]) && (bitmask & EST)))
    {
        return NULL;
    }

    return &dam->cases[orig->index + direction_delta(dir, vois)];
}

/**
 * -> pointeur vers la case vide de la colonne donnée.
 * Appeller cette fonction sur une colonne remplie est une erreur.
 * Verifier que la premiere case soit bien vide avant.
 */
struct empla * case_vide(struct dam * dam, size_t colonne) {
    struct empla * curs = empla(dam, colonne), * cible;

    while ((cible = vers(dam, curs, DIR_S)) != NULL && cible->contenu == CASE_VIDE) {
        curs = cible;
    }

    return curs;
}

/**
 * -> poids de la case demandée.
 */
size_t poids_case(struct joueur *jr, struct dam * dam, struct empla *orig) {
    size_t long_, long_cap, i_dir, i_sens;
    _Bool vide_trouve;
    char ct;
    struct empla * cur;
    size_t taille[] = { 1, 1, 1, 1 }, cap[] = { 1, 1, 1, 1 }, poids = 0;

    for (i_dir = 0; i_dir < NOMBRE_DIRECTIONS; i_dir++) {
        long_ = long_cap = 0;
        vide_trouve = 0;
        cur = orig;
        while ((cur = vers(dam, cur, i_dir)) != NULL
        && ((ct = cur->contenu) == CASE_VIDE || ct == jr->symbole)) {

            if (ct == CASE_VIDE) {
                vide_trouve = 1;
            }

            if (!vide_trouve) {
                long_++;
            }

            long_cap++;
        }

        taille[i_dir / 2] += long_;
        cap[i_dir / 2] += long_cap;
    }

    for (i_sens = 0; i_sens < 4; i_sens++) {
        if (cap[i_sens] < LONGUEUR_VICTOIRE) {
            taille[i_sens] = 0;
        }
    }

    for (i_sens = 0; i_sens < 4; i_sens++) {
        poids += puissance_10(taille[i_sens] - 1);
    }

    return poids;
}

enum poids_colonnes {
    COLONNE_REMPLIE = -1999,
    FAIT_GAGNER_UN_AUTRE = -42,
};
static_assert (COLONNE_REMPLIE < FAIT_GAGNER_UN_AUTRE, "Le poids d'une "
   "colonne remplie doit etre inferieur a tous les autres poids de sorte "
   "qu'elle ne soit jamais selectionne par l'IA et provoque un SEGFAULT");

/**
 * Poids qui sera perçu par l'IA pour chaque colonne du damier pour un joueur donné
 */
typedef size_t table_poids[LARGEUR_DAMIER];
void ponderer(struct dam * dam, struct joueur * jr, table_poids * pnd) {
    size_t i;

    for (i = 0; i < LARGEUR_DAMIER; i++) {
        if (empla(dam, i)->contenu != CASE_VIDE) {
            (*pnd)[i] = COLONNE_REMPLIE;
            continue;
        }

        (*pnd)[i] = poids_case(jr, dam, case_vide(dam, i));
    }
}

enum raisons {
    AUCUNE_RAISON,
    COUP_GAGNANT,
    CONTRE,
    SON_MEILLEUR,
    SAIT_PERDANT,
};

char * raisons[] = {
    [AUCUNE_RAISON] = "Aucune raison",
    [COUP_GAGNANT] = "Sienne gagnante",
    [CONTRE] = "Contre l'ennemi",
    [SON_MEILLEUR] = "Son meilleur coup",
    [SAIT_PERDANT] = "Sait qu'il a perdu",
};

#if defined(TESTING_PUISSANCE4)
void afficher_ponderation(struct joueur * jr, table_poids * pnd, enum raisons raison) {
    size_t * p;

    printf("%s :\n", raisons[raison]);
    printf(
        "Poids pour le joueur %zd (%c)\n",
        jr->index,
        jr->symbole
    );
    for (p = &((*pnd)[0]); p < &((*pnd)[LARGEUR_DAMIER]); p++) {
        printf(
            "Poids colonne %2zd : %zd\n"
            , LARGEUR_DAMIER + 1 - (&((*pnd)[LARGEUR_DAMIER]) - p),
            (*p)
        );
    }
    fflush(stdout);
}
#endif

/* -> un numéro de colonne, et une raison de jeu */
static enum raisons raison_ia;

size_t choix_ia(struct dam * dam, struct joueur * soi, struct joueur * ennemi_direct) {
    table_poids pnd_soi;
    table_poids pnd_enn;
    table_poids pnd_virt;
    ponderer(dam, soi, &pnd_soi);
    ponderer(dam, ennemi_direct, &pnd_enn);
    struct dam dam_virt;
    size_t i_col;
    size_t col_choisie;

    raison_ia = AUCUNE_RAISON;

    if (poids_est_gagnant(MEILLEUR(pnd_soi, 0))) {
        col_choisie = MEILLEURIDX(pnd_soi, 0);
        raison_ia = COUP_GAGNANT;
    }
    else {
        for (i_col = 0; i_col < LARGEUR_DAMIER; i_col++) {
            if (empla(dam, i_col)->contenu != CASE_VIDE) continue;
            dam_virt = *dam;
            case_vide(&dam_virt, i_col)->contenu = soi->symbole;
            ponderer(&dam_virt, ennemi_direct, &pnd_virt);
            if (poids_est_gagnant(MEILLEUR(pnd_virt, 0))) {
                pnd_soi[i_col] = FAIT_GAGNER_UN_AUTRE;
                pnd_enn[i_col] = FAIT_GAGNER_UN_AUTRE;
            }
        }

        if (MEILLEUR(pnd_enn, 0) > MEILLEUR(pnd_soi, 0)) {
            col_choisie = MEILLEURIDX(pnd_enn, 0);
            raison_ia = CONTRE;
        }
        else {
            col_choisie = MEILLEURIDX(pnd_soi, 0);
            raison_ia = SON_MEILLEUR;

            if(pnd_soi[col_choisie] == FAIT_GAGNER_UN_AUTRE) {
                raison_ia = SAIT_PERDANT;
            }
        }

        afficher_ponderation(ennemi_direct, &pnd_enn, CONTRE);
    }

    afficher_ponderation(soi, &pnd_soi, SON_MEILLEUR);

    return col_choisie;
}

void afficher_numeros_ligne(void) {
    size_t i;
    for (i = 1; i <= LARGEUR_DAMIER; i++) {
        printf("  %zd ", i);
    }
    putchar('\n');
}

void afficher_separation_horizontale(void) {
    size_t i;

    for (i = 1; i <= LARGEUR_DAMIER; i++) {
        printf("+---");
    }
    puts("+");
}

void afficher_damier(struct dam * dam) {
    size_t i;

    for (i = 0; i < TAILLE_DAMIER; i++) {

        if (i == 0) {
            afficher_numeros_ligne();
            afficher_separation_horizontale();
        }

        printf("| %c ", empla(dam, i)->contenu);
        fflush(stdout);

        if (((i + 1) % LARGEUR_DAMIER) == 0) {
            puts("|");
            afficher_separation_horizontale();
        }
    }
    afficher_numeros_ligne();
}

int main(void) {
    int seed;
    size_t colonne_jouee;
    struct empla * case_jouee;
    size_t i;
    char lettre;

    puts("Entrez une seed (0-99) ou appuyez sur [Entree] pour une seed aléatoire.");
    seed = demander_nombre_naturel() % 100;
    if(seed < 0) {
        time_t the_time = time(NULL);

        if (the_time == -1) {
            puts("Cannot initialize the time");
            return EXIT_FAILURE;
        }

        seed = (int)the_time % 100;
    }
    srand((unsigned int)seed);

    printf("Seed: %d\n", seed);

    struct jeu jeu = {
        .dam.bords = {
            [BORD_HAUT] = 0,
            [BORD_GAUCHE] = 0,
            [BORD_BAS] = HAUTEUR_DAMIER - 1,
            [BORD_DROIT] = LARGEUR_DAMIER - 1,
        },
        .dam.voisins = {
            [DIR_N].bitmask = NORD,
            [DIR_N].delta = -LARGEUR_DAMIER,
            [DIR_S].bitmask = SUD,
            [DIR_S].delta = +LARGEUR_DAMIER,
            [DIR_O].bitmask = OUEST,
            [DIR_O].delta = -1,
            [DIR_E].bitmask = EST,
            [DIR_E].delta = +1,
            [DIR_NO].bitmask = NORD | OUEST,
            [DIR_NO].delta = -LARGEUR_DAMIER - 1,
            [DIR_SE].bitmask = SUD | EST,
            [DIR_SE].delta = +LARGEUR_DAMIER + 1,
            [DIR_NE].bitmask = NORD | EST,
            [DIR_NE].delta = -LARGEUR_DAMIER + 1,
            [DIR_SO].bitmask = SUD | OUEST,
            [DIR_SO].delta = +LARGEUR_DAMIER - 1,
        },
    };

    joueur(&jeu, 0)->symbole = CASE_JOUEUR_1;
    joueur(&jeu, 1)->symbole = CASE_JOUEUR_2;
    joueur(&jeu, 0)->index = 0;
    joueur(&jeu, 1)->index = 1;

    struct joueur * joueur_actif = &(jeu.joueurs[0]);

    for (i = 0; i < TAILLE_DAMIER; i++) {
        jeu.dam.cases[i] = (struct empla) {
            .contenu = CASE_VIDE,
            .index = i,
            .colonne = i % LARGEUR_DAMIER,
            .ligne = i / LARGEUR_DAMIER,
        };
    }

    afficher_damier(&jeu.dam);

    for (i = 0; i < NOMBRE_JOUEURS; i++) {
        do {
            printf("\nJoueur %zd humain ou ordinateur ? (h/o)\n> ", i + 1);
            lettre = demander_lettre();
            joueur(&jeu, i)->humain = (lettre == 'h');
        } while (lettre != 'h' && lettre != 'o');
    }

#if defined(TESTING_PUISSANCE4)
    /* p10 */
    assert(puissance_10(1) == 10);
    assert(puissance_10(2) == 100);
    assert(puissance_10(0) == 1);
    assert(puissance_10(-1) == 0);
    /* Meilleurs */
    long test[6] = { 1, 2, 3, 4, 5, 6 };
    assert(MEILLEURIDX(test, 0) == 5);
    long test2[6] = { 1, 2, 1000, 4, 5, 6 };
    assert(MEILLEURIDX(test2, 0) == 2);
    struct test {
        char a; _Bool b : 1; long val;
        struct test2 {
            char foo; long val2;
        } test2;
    } test_struct[4] = {
        (struct test) { 'a', 0, 1, (struct test2) { 'a', 1 }},
        (struct test) {'a', 0, 52, (struct test2) { 'a', 1 }},
        (struct test) {'a', 1, 5, (struct test2) { 'a', 120 }},
        (struct test) {'a', 0, 42, (struct test2) { 'a', 12 }},
    };
    assert(MEILLEURIDX(test_struct, offsetof(struct test, val)) == 1);
    assert(MEILLEURIDX(test_struct, offsetof(struct test, test2.val2)) == 2);
#if defined(MOCK_PUISSANCE4)
    /* Poids */
    case_vide(&jeu.damier, 0)->contenu = CASE_JOUEUR_1;
    assert(poids_case(&jeu.joueurs[0], empl(&jeu.damier, LARGEUR_DAMIER * (HAUTEUR_DAMIER - 1))) == 3);
#endif
    /* Voisins */
    struct empla * case_droite = empla(&jeu.dam, LARGEUR_DAMIER * 2 - 1);
    assert(vers(&jeu.dam, case_droite, DIR_N) == empla(&jeu.dam, LARGEUR_DAMIER - 1));
    assert(vers(&jeu.dam, case_droite, DIR_NE) == NULL);
    assert(vers(&jeu.dam, case_droite, DIR_E) == NULL);
    assert(vers(&jeu.dam, case_droite, DIR_SE) == NULL);
    assert(vers(&jeu.dam, case_droite, DIR_S) == empla(&jeu.dam, LARGEUR_DAMIER * 3 - 1));
    assert(vers(&jeu.dam, case_droite, DIR_SO) == empla(&jeu.dam, LARGEUR_DAMIER * 3 - 2));
    assert(vers(&jeu.dam, case_droite, DIR_O) == empla(&jeu.dam, LARGEUR_DAMIER * 2 - 2));
    assert(vers(&jeu.dam, case_droite, DIR_NO) == empla(&jeu.dam, LARGEUR_DAMIER - 2));
#endif

    for (;;) {
        afficher_damier(&jeu.dam);

        if (joueur(&jeu, joueur_actif->index)->humain) {
            printf(
                "Au joueur %zd (%c) de jouer (un chiffre entre 1 et %d)\n> ",
                joueur_actif->index + 1,
                joueur_actif->symbole,
                LARGEUR_DAMIER
            );

            for (;;) {
                colonne_jouee = (size_t)demander_nombre_naturel() - 1;

                if (colonne_jouee >= 0 && colonne_jouee < LARGEUR_DAMIER) {
                    if (empla(&jeu.dam, colonne_jouee)->contenu == CASE_VIDE) {
                        break;
                    }
                    else {
                        puts("Colonne remplie, veuillez en choisir une autre.");
                    }
                }
                else {
                    puts("Chiffre invalide.");
                }
                printf("Veuillez renseigner une autre valeur\n> ");
            }
        }
        else {
            colonne_jouee = choix_ia(&jeu.dam, joueur_actif, prochain_joueur(&jeu, joueur_actif));
        }

        printf("Le joueur %zd (%c), (%s) joue à la colonne %zd%s%s\n",
           joueur_actif->index + 1,
           joueur_actif->symbole,
           joueur_actif->humain ? "humain" : "ordinateur",
           colonne_jouee + 1,
           joueur_actif->humain ? "" : " - ",
           joueur_actif->humain ? "" : raisons[raison_ia]
        );

        MACRO_PRESS_ENTER_TO_CONTINUE;

        case_jouee = case_vide(&jeu.dam, colonne_jouee);
        case_jouee->contenu = joueur_actif->symbole;

        if (poids_est_gagnant(poids_case(joueur_actif, &jeu.dam, case_jouee))) {
            joueur_actif->gagnant = 1;
            break;
        }

        jeu.egalite = 1;

        for (i = 0; i < LARGEUR_DAMIER; i++) {
            if (empla(&jeu.dam, i)->contenu == CASE_VIDE) {
                jeu.egalite = 0;
                break;
            }
        }
        if (jeu.egalite) {
            break;
        }

        joueur_actif = prochain_joueur(&jeu, joueur_actif);
    }

    if (jeu.egalite) {
        puts("Egalité !");
    }
    else {
        for (i = 0; i < NOMBRE_JOUEURS; i++) {
            if (joueur(&jeu, i)->gagnant) {
                printf("Le joueur %zd (%c) a gagné\n",
                    joueur_actif->index + 1,
                    joueur_actif->symbole
                );
            }
        }
    }

    afficher_damier(&jeu.dam);
    printf("Seed: %d\nEntrez la lettre q pour quitter\n", seed);

    while(demander_lettre() != 'q');

    return EXIT_SUCCESS;
}